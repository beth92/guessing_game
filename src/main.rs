use std::io;
use std::cmp::Ordering;
use rand::Rng;

fn main() {
    println!("Guess the number!");

    // thread_rng gets a generator local to the current thread and seeded by OS
    // gen_range is (inclusive, exclusive)
    let secret_number = rand::thread_rng().gen_range(1, 101);

    loop {
        println!("Please input your guess:");

        let mut guess = String::new(); // heap-allocated String type provided by std::string
        // new is a static method on the String type

        // we pass a REFERENCE to guess to the readline fn
        // read_line returns a type io::Result which is an enum with variants Err or Ok
        io::stdin().read_line(&mut guess)
            // if Result type is err, prints message, otherwise returns Result value of Ok
            .expect("Failed to read line");

        // shadowing is useful when we want to convert the type of a value
        // use trim to get rid of the \n from read_line
        // run a match on the Result if you don't want to crash upon Err
        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => {
                println!("Please enter a valid whole number between 1 and 100.");
                continue
            }
        };

        // note: if result is missing, compiler will warn that error handling may be incomplete

        println!("You guessed {}", guess);

        // cmp = comopare and Ordering is an enum with variants Less, Greater, Equal
        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too small!"),
            Ordering::Greater => println!("Too big!"),
            Ordering::Equal => {
                println!("You win!");
                break;
            }
        }
    }
}
